import java.net.*;
import java.io.*;

/** author : Adarsh Jegadeesan - 344942
	* The DictClient takes in as inputs the word, host and the server port
  * number
**/

public class DictClient
{
	public static void main(String args[]){
		// args give message contents, server hostname and port number
		RemoteDictionary dictionary;
		long startTime = System.nanoTime();
		int ServerPort = Integer.parseInt(args[2]);
		// create new dictionary
		dictionary = new RemoteDictionary(ServerPort,args[1]);
		// look for word in the dictionary
		String result = dictionary.search(args[0]);
		// print out meaning
		System.out.println(result);     
		long endTime = System.nanoTime();;
//	System.out.println("Took "+(endTime - startTime) + " ns");
	}
}
