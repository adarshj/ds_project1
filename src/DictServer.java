import java.net.*;
import java.io.*;
import java.util.concurrent.*;

/** Author : Adarsh Jegadeesan - adarshj
  * The DictServer class 
  * inputs - the server port number, the dictionary file containing the 
  * words and their corresponding meanings.
  * Creates a worker pool with a fixed number of threads and execute this 
  * pool of threads once a request is received.
**/ 
public class DictServer
{
	public static void main(String args[])
	{
		WorkerThread worker;
		DatagramSocket Serversocket = null;
		byte[]buffer = new byte[1000];
		// create the thread pool
		ExecutorService pool = Executors.newFixedThreadPool(10);
		try {
			int portno = Integer.parseInt(args[0]);
			// ensure not a reserved port
			if (portno < 1024){
				throw new IllegalArgumentException(portno + "is not greater than or equal to 1024"); 
			}
      // create server socket to accept connections
			Serversocket = new DatagramSocket(portno);
			while (true){
				// the request
				DatagramPacket request = new DatagramPacket(buffer,buffer.length);
				Serversocket.receive(request);   
				// create worker threads to handle request
				pool.execute(new WorkerThread(Serversocket,request,args[1]));
			}
		} catch (SocketException e){
			System.out.println("Socket:" + e.getMessage());
		}	catch (IOException e){
			System.out.println("IO:"+  e.getMessage());
		} finally {
			if (Serversocket != null)Serversocket.close();
		}
	}
} 

