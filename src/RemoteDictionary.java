import java.net.*;
import java.io.*;

/** Author : Adarsh Jegadeesan - adarshj
	* RemoteDictionary class - Sends the client request to the server
  * by creating client sockets. Using these sockets they send the
  * server responses back to the client.
**/  

public class RemoteDictionary
{

  DatagramSocket ClientSocket = null; // client socket
  String address;
  int port;
  
  public RemoteDictionary(int port, String address){
    this.port = port;
    this.address = address;
  }
  
  // Method used to make request to the server
  public String search(String Word){
		String result;
    try {
      // create socket to send and receive data
      ClientSocket = new DatagramSocket();
      byte[] m = Word.getBytes();
      InetAddress Host = InetAddress.getByName(address);
      DatagramPacket request = new DatagramPacket(m,Word.length(),
			Host,port);
      ClientSocket.send(request);
      byte[] buffer = new byte[1000];
      DatagramPacket reply = new DatagramPacket(buffer,buffer.length);
      ClientSocket.receive(reply);
      // covert the result to String
      result = new String(reply.getData());
			return result;
    }
    catch (SocketException e){System.out.println("Socket:" + e.getMessage());}
    catch (IOException e){System.out.println("IO:" + e.getMessage());}
    finally {
        if (ClientSocket != null)ClientSocket.close();
    }
		
		return null;


  }
}

