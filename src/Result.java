import java.util.*;
import java.io.*;

/** Author : Adarsh Jegadeesan - adarshj 
 * Result class - Find the requested word in the dictionary
 * and return its corresponding meaning.
**/


public class Result {
  
	String dictfile; // the file containing the dictionary
  String temp;
  String delimiter = "-"; // The separator between the word and the meaning

  public Result(String dictfile){
    this.dictfile = dictfile;
  }
 
  // go over the dictionary and find the word and its corresponding meaning
  public String find (String word){
    try {
      Scanner ScanObj = new Scanner(new FileReader(dictfile));
      while(ScanObj.hasNext()) {
				temp = ScanObj.nextLine();
        String[] tokens = temp.split(delimiter);
				if (tokens[0].equals(word)){
          return tokens[1];
				}
      }
		// return error message if file not found
    } catch (FileNotFoundException e){System.out.println(e.getMessage());};
		// no such word found
    return "Sorry, word does not exist!";	

 } 
}
