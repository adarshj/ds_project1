import java.net.*;
import java.io.*;
import java.util.concurrent.*;

/** Author: Adarsh Jegadeesan - adarshj
  * The worker thread class -  is in charge of handling the requests from 
  * various clients and responding to the individual clients. 
  *
**/

class WorkerThread implements Runnable{

	DatagramSocket Serversocket;
	DatagramPacket request;
	protected Thread runningThread = null; // current thread
	Result result;
	String DictFile;

	public WorkerThread(DatagramSocket Serversocket, DatagramPacket request, 
  String DictFile){
		this.Serversocket = Serversocket;
		this.request = request;
		this.DictFile = DictFile;
	}
  // run thread
	public void run(){
    // ensure only one thread running at a particuar instance
    synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		try {
			byte[]buffer = new byte[1000];
      // get request
			buffer = request.getData();
			// create new result
			Result result = new Result(DictFile);
			String word = new String(buffer, 0, request.getLength());
			// look for meaning of the word
			String meaning = result.find(word); 
			DatagramPacket reply = new DatagramPacket(meaning.getBytes(), 
			meaning.length(), request.getAddress(), request.getPort());
			// send reply
			Serversocket.send(reply);
		} catch (IOException e) {
			System.out.println("IO:" + e.getMessage());
		}
	}	
}    	

