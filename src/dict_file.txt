artistic-showing skill or excellence in execution
blow-a sudden, hard stroke with a hand, fist, or weapon
carriage-A means of conveyance, in particular.
drown-to die under water or other liquid of suffocation. 
elegant-tastefully fine or luxurious in dress
facet-one of the small, polished plane surfaces of a cut gem. 
glue-a hard, impure, protein gelatin, obtained by boiling skins, hoofs, and other animal substances in water, that when melted or diluted is a strong adhesive. hill-a natural elevation of the earth's surface, smaller than a mountain. 
irregular-without symmetry, even shape, formal arrangement
jelly-a food preparation of a soft, elastic consistency due to the presence of gelatin, pectin.

